import schedule
import time
import json, requests
import linecache
from flask import Flask, render_template
import random as r
import random            
import socket
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import time
import threading
import hashlib
from tronpy import Tron
from tronpy.keys import PrivateKey
import linecache
#create interval that change array related to hashrate yes yes
import schedule
import time
import requests, json
import ast
from flask import request
from decimal import Decimal

# f = open("payments.txt", "r")
# content = f.read()
# content_lsit = content.split("\n")
# array_to_fillup = 0 


DUCO_cloud_password = ""

trx_addr_cloud = ""

TRX_cloud_Private_key = ""

def payments():
    with open('payments.txt') as file:
        lines = file.readlines()
    for number, line in enumerate(lines, 1):
        offer_hash_var = "offer"
        if offer_hash_var in line:
            print(f'{offer_hash_var} is in the line {number}') 
            line_payments = linecache.getline("payments.txt", number)
            print(line_payments)  

            array_line_payments_striped = line_payments.split(",")
            duco_miner_var = array_line_payments_striped[0]
            duco_bidder_var = array_line_payments_striped[1]
            TRX_cloud_Private_key = array_line_payments_striped[2]
            trx_addr_cloud =  array_line_payments_striped[3]
            trx_addr_miner = array_line_payments_striped[4]
            price_per_day = int(array_line_payments_striped[5])


            print("Duco miner: ", duco_miner_var)
            print("Duco buying: ", duco_bidder_var)
            print("TRX Private key: ", TRX_cloud_Private_key)
            print("Trx addr from: ", trx_addr_cloud)
            print("Trx addr to: ", trx_addr_miner)
            print("Price per day: ", price_per_day)
            
            print(trx_addr_cloud)
            print(TRX_cloud_Private_key)
            
            real_trx_price_perday = price_per_day + 999999 # 100_000 = 1TRX
            client = Tron()
            
            priv_key = PrivateKey(bytes.fromhex("{}".format(TRX_cloud_Private_key)))

            txn = (
                client.trx.transfer(trx_addr_cloud, trx_addr_miner, real_trx_price_perday)
                .memo("Furime services")
                .build()
                .sign(priv_key)
                    )
            print(txn.txid)
            print(txn.broadcast().wait())
            
            response_balance = requests.get('https://server.duinocoin.com/balances/{}'.format(duco_miner_var))
            normalized_response_balance = response_balance.text
            
            
            y = json.loads(response_balance.text)
            not_splitted_var = y["result"]["balance"]
            int_not_splitted_var = int(not_splitted_var)
            response = requests.get('https://server.duinocoin.com/transaction/?username={}&password={}&recipient={}&amount={}&memo=Furime_Services'.format(duco_miner_var, DUCO_cloud_password, duco_bidder_var, int_not_splitted_var))
            print(response)
            print(int_not_splitted_var)
            # print(response.text)

def clear_payments():
    open("payments.txt", "w").close()


schedule.every().day.at("00:00").do(payments)

schedule.every().monday.do(clear_payments)

while True:
    schedule.run_pending()
    time.sleep(1)
